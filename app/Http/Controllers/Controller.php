<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Response;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
   	{
   	 	$airports = json_decode(file_get_contents('https://api.ryanair.com/aggregate/4/common?embedded=airports&market=es-es'), true);
   	 	//dd($airports);
        return view('airports',['airports'=> $airports]);
   	}

   	public function index2()
   	{
   	 	$airports = json_decode(file_get_contents('https://api.ryanair.com/aggregate/4/common?embedded=airports&market=es-es'), true);
   	 	//dd($airports);
        return view('relation',['airports'=> $airports]);
   	}

    public function index3()
    {
      $airports = json_decode(file_get_contents('https://api.ryanair.com/aggregate/4/common?embedded=airports&market=es-es'), true);
      //dd($airports);
      return view('relationdistance',['airports'=> $airports]);
    }

    public function show()
    {
      //set the distance array
        $_distArr = array();
        $_distArr[1][2] = 7;
        $_distArr[1][3] = 9;
        $_distArr[1][6] = 14;
        $_distArr[2][1] = 7;
        $_distArr[2][3] = 10;
        $_distArr[2][4] = 15;
        $_distArr[3][1] = 9;
        $_distArr[3][2] = 10;
        $_distArr[3][4] = 11;
        $_distArr[3][6] = 2;
        $_distArr[4][2] = 15;
        $_distArr[4][3] = 11;
        $_distArr[4][5] = 6;
        $_distArr[5][4] = 6;
        $_distArr[5][6] = 9;
        $_distArr[6][1] = 14;
        $_distArr[6][3] = 2;
        $_distArr[6][5] = 9;

        //the start and the end
        $a = 1;
        $b = 5;
      return view('dijkstra',['_distArr' => $_distArr, 'a' => $a, 'b' => $b]);
    }

    public function show2()
    {
      $start = request()->inicio;
      $fin = request()->fin;
      
      return view('dijkstra');
    }
}
