<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Aeropuertos', 'Controller@index');

Route::get('/Relacion', 'Controller@index2');

Route::get('/RelacionDistancia', 'Controller@index3');

Route::get('/Dijkstra', 'Controller@show');

Route::post('/Dijkstra', 'Controller@show2');