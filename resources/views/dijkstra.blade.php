@extends ('layouts.app')
 
@section('content')

<?php
        //initialize the array for storing
        $S = array();//the nearest path with its parent and weight
        $Q = array();//the left nodes without the nearest path
        foreach(array_keys($_distArr) as $val) $Q[$val] = 99999;
        $Q[$a] = 0;

        while(!empty($Q)){
            $min = array_search(min($Q), $Q);//the most min weight
            if($min == $b) break;
            foreach($_distArr[$min] as $key=>$val) if(!empty($Q[$key]) && $Q[$min] + $val < $Q[$key]) {
                $Q[$key] = $Q[$min] + $val;
                $S[$key] = array($min, $Q[$key]);
            }
            unset($Q[$min]);
        }

        //list the path
        $ruta = array();
        $pos = $b;
        while($pos != $a){
            $ruta[] = $pos;
            $pos = $S[$pos][0];
        }
        $ruta[] = $a;
        $ruta = array_reverse($ruta);
?>
    <h1 align="center">Dijkstra</h1>
    <body>
        <center>
        "<img src='Dijkstra_Animation.gif'>"
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Puntos del recorrido</th>
                <th>Distancia</th>
                <th>Ruta</th>
            </tr>
        </thead>
        <tbody>
            <th>{{ $a }} -> {{ $b}}</th>
            <th>{{ $S[$b][1] }}</th>
            <th>{{ implode('->', $ruta) }}</th>
        </tbody>
        </table>
        </center>
    </body>
@endsection