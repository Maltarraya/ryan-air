<!doctype html>
<html>
    <head>
        <title>Inteligencia artiificial</title>
        <link rel="stylesheet" href="css/app.css" />
    </head>
    <body>
        <nav class="navbar navbar-default">
                <div>
                    <ul class="nav navbar-nav">
                        <li class = "{{ Request::is('Aeropuertos') ? 'active' : ''}}"><a href="{{ Action('Controller@index') }}">Aeropuertos</a></li>
                        <li class = "{{ Request::is('Relacion') ? 'active' : '' }}"><a href="{{ Action('Controller@index2') }}">Relacion entre aeropuertos</a></li>
                        <li class = "{{ Request::is('RelacionDistancia') ? 'active' : '' }}"><a href="{{ Action('Controller@index3') }}">Relacion con distancia</a></li>
                        <li class = "{{ Request::is('Dijkstra') ? 'active' : '' }}"><a href="{{ Action('Controller@show') }}">Dijkstra</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        <div>
            @yield ('content')
        </div>
    </body>
    <script src="js/app.js"></script>
</html>