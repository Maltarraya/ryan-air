<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <link rel="stylesheet" href="css/app.css" />
    <h1 align="center">Relaciones</h1>
    <body>
        <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                  @foreach ($airports as $airports2)
                        @foreach ($airports2 as $airport)
                            <th>
                                <t width="200%">{{ $airport['name'] }}</t>
                            </th>
                        @endforeach
                  @endforeach
        </thead>
        <tbody>
            @foreach ($airports as $airports2)
                @foreach ($airports2 as $airport)
                <tr>
                    <th>
                        <t>{{ $airport['name'] }}</t>
                    </th>
                        @foreach ($airports as $airports3)
                            @foreach ($airports3 as $airports4)
                                <?php
                                $j = count($airports4['routes']);
                                $found = '0';
                                $search = 'airport:'. $airport['iataCode'];
                                for ($i=0;$i<$j;$i++)
                                {   
                                    
                                    if($airports4['routes'][$i] == $search)
                                    {
                                        $found = '1';
                                    }
                                    else{}
                                }
                                ?>
                                <th>
                                    <center><t>{{ $found }}</t></center>
                                </th>
                            @endforeach
                        @endforeach
                </tr>
                @endforeach
             @endforeach
        </tbody>
    </table>
    </body>
    <script src="js/app.js"></script>
</html>
