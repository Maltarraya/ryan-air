<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <link rel="stylesheet" href="css/app.css" />
    <h1 align="center">Relaciones</h1>
    <body>
        <table class="table table-hover">
        <thead>
            <tr>
                <th>#</th>
                  @foreach ($airports as $airports2)
                        @foreach ($airports2 as $airport)
                            <th>
                                <t width="200%">{{ $airport['name'] }}</t>
                            </th>
                        @endforeach
                  @endforeach
        </thead>
        <tbody>
            @foreach ($airports as $airports2)
                @foreach ($airports2 as $airport)
                <tr>
                    <th>
                        <t>{{ $airport['name'] }} - {{ $airport['priority'] }}</t>
                    </th>
                        @foreach ($airports as $airports3)
                            @foreach ($airports3 as $airports4)
                                <?php
                                $j = count($airports4['routes']);
                                $dd = '-';
                                $search = 'airport:'. $airport['iataCode'];
                                for ($i=0;$i<$j;$i++)
                                {   
                                    
                                    if($airports4['routes'][$i] == $search)
                                    {
                                        $airport1 = $airport['priority'];
                                        $airport2 = $airports4['priority'];
                                        $found = '1';
                                        $lat1 = $airport['coordinates']['latitude'];
                                        $long1 = $airport['coordinates']['longitude'];
                                        $lat2 = $airports4['coordinates']['latitude'];
                                        $long2 = $airports4['coordinates']['longitude']; 
                                        $km = 111.302;
                                        $degtorad = 0.01745329;
                                        $radtodeg = 57.29577951; 
                                        $dlong = ($long1 - $long2); 
                                        $dvalue = (sin($lat1 * $degtorad) * sin($lat2 * $degtorad)) + (cos($lat1 * $degtorad) * cos($lat2 * $degtorad) * cos($dlong * $degtorad)); 
                                        $dd = acos($dvalue) * $radtodeg; 
                                        $dd = round(($dd * $km), 2);
                                        $array [$airport1][$airport2] = $dd;
                                        $dd = $dd . 'km';
                                    }
                                    else{}
                                }
                                ?>
                                <th>
                                    <center><t>{{ $dd }}</t></center>
                                </th>
                            @endforeach
                        @endforeach
                </tr>
                @endforeach
             @endforeach
        </tbody>
    </table>
    <form action="{{ Action('Controller@show2') }}" method="POST" class="navbar-form navbar-nav navbar-right" role = "form">
            {{ csrf_field() }}
            <div class = "form-group col-md-2">
                <label for="inicio">Inicio</label>
                <input type="number" step="any" name='inicio' class="form-control" id="inicio" required>
                <label for="fin">Fin</label>
                <input type="number" step="any" name='fin' class="form-control" id="fin" required> 
                <input type="hidden" name='datos' class="form-control" id="datos" value='{{ $airports }}'> 
                <button type="submit" class="btn btn-default btn-lg">Solicitar ruta</button>
            </div>
    </body>
    <script src="js/app.js"></script>
    }
</html>
