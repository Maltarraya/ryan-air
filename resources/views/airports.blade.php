@extends ('layouts.app')
 
 @section('content')
    <h1 align="center">Aeropuertos</h1>
    <body>
        <table class="table table-hover">
        <thead>
            <tr>
                <th>Aeropuerto</th>
                <th>latitude</th>
                <th>longitude</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($airports as $airports2)
            
                @foreach ($airports2 as $airport)
                <tr>
                    <th>
                        <t>{{ $airport['name'] }}</t>
                    </th>
                    <th>
                        <t>{{ $airport['coordinates']['latitude'] }}</t>
                    </th>
                    <th>
                        <t>{{ $airport['coordinates']['longitude'] }}</t>
                    </th>
                </tr>
                @endforeach
             @endforeach
        </tbody>
    </table>
    </body>
@endsection
